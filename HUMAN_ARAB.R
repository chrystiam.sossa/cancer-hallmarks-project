#####################################################################################################
#####################################################################################################
#####################################################################################################
#########################                                                  ##########################
######################### FINDING ORTHOLOGUES USING GENE IDS AND FASTA FILES VIA BLAST HUMAN -> ARAB#
########################## CCSA 2020                                        #########################
########################## OMICAS PROJECT                                   #########################
#####################################################################################################
#####################################################################################################
#####################################################################################################

#########################
#INSTALLING IF IT IS REQUIRED
# if (!requireNamespace("BiocManager", quietly = TRUE))
#   install.packages("BiocManager")
# 
# BiocManager::install("org.At.tair.db")
# BiocManager::install("PANTHER.db")
#devtools::install_github("mhahsler/rBLAST")
#devtools::install_github("HajkD/orthologr")
#########################
#CALLING LIBRARIES
library(biomaRt)
library(org.At.tair.db)
library(rentrez)
library(PANTHER.db)
library(AnnotationHub)
library(tidyr)
library(dplyr)
# library("hoardeR")
library(stringr)
library(Biostrings)
library(stringi)
library(data.table)
library(xml2)
library(rvest)
library(RCurl)
library(rlist)
require(data.table)
#########################
`%notin%` <- Negate(`%in%`)
#https://bioconductor.org/packages/release/data/annotation/vignettes/PANTHER.db/inst/doc/PANTHER.db.html
#https://ropensci.org/tutorials/rentrez_tutorial/
#ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.2.30/
#http://nebc.nerc.ac.uk/bioinformatics/documentation/blast+/user_manual.pdf
#https://www.arabidopsis.org/download/index-auto.jsp?dir=%2Fdownload_files%2FSequences%2FTAIR10_blastsets # TAIR10_cdna_20101214_updated  
#https://www.arabidopsis.org/download_files/Sequences/TAIR10_blastsets/TAIR10_cdna_20101214_updated
#https://cran.r-project.org/web/packages/biomartr/vignettes/Sequence_Retrieval.html
#https://palfalvi.org/post/local-blast-from-r/
#https://angus.readthedocs.io/en/2019/visualizing-blast-scores-with-RStudio.html
#https://www.ncbi.nlm.nih.gov/books/NBK279684/#_appendices_Options_for_the_commandline_a_
#########################
path <- "E:/JAVERIANA/CANCER"
workspace_path <- paste0(path,"/","workspace");if(!file.exists(workspace_path)){dir.create(workspace_path)}
output_dir <- paste0(workspace_path,"/","out");if(!file.exists(output_dir)){dir.create(output_dir)}
#########################
tableFolder_H <- paste0(output_dir,"/","TABLE_HUMAN");if(!file.exists(tableFolder_H)){dir.create(tableFolder_H)}
xmlFolder_H <- paste0(output_dir,"/","XML_HUMAN");if(!file.exists(xmlFolder_H)){dir.create(xmlFolder_H)}
logFolder_H <- paste0(output_dir,"/","LOG_HUMAN");if(!file.exists(logFolder_H)){dir.create(logFolder_H)}
SeqFolder_H <- paste0(output_dir,"/","FASTA_ENSEMBL_HUMAN");if(!file.exists(SeqFolder_H)){dir.create(SeqFolder_H)}
#SeqFolder_ENTREZ <- paste0(output_dir,"/","FASTA_ENTREZ");if(!file.exists(SeqFolder_ENTREZ)){dir.create(SeqFolder_ENTREZ)}

# ##########################
# #GET ANNOTATED PANTHER DB AND NCBI KEY
# ah <- AnnotationHub()
# query(ah, "PANTHER.db")[[1]]
# # key_NCBI <- "3208f1eeb984a1b7ebf5d567133d9caf8909"
#########################
#########################
#ESTABLISHING CONNECTIONS TO ENSEMMBL
#ensembl = biomaRt::useMart("ensembl",host="www.ensembl.org")
ensembl = biomaRt::useEnsembl("ensembl",mirror="useast")
ensembl = biomaRt::useDataset("hsapiens_gene_ensembl", mart = ensembl) #,

#FILTERING TO HUMAN RECORDS
# availablePthOrganisms(PANTHER.db)[1:5,]
#pthOrganisms(PANTHER.db) <- "HUMAN"# c("HUMAN") #"ARABIDOPSIS"
#########################
# resetPthOrganisms(PANTHER.db)
# columns(PANTHER.db)
# keytypes(PANTHER.db)
#########################
#READING ORIGINAL QUERY OF GENES


#ICGC FILE
# file_to_read <- read.table(paste0(path,"/","Protein coding_Reactome Pathways.tsv"))
# colnames(file_to_read) <- c("ensembl_gene_id","external_gene_name")

#NCG6
file_to_read <- as.data.frame(data.table::fread(paste0(path,"/","NCG6_cancergenes.tsv")))
#GETTING UNIQUE GENE IDS FROM ENSEMBL USING ENTREZ ID AND AVOID USE RENTREZ
entrzID <- unique(file_to_read$entrez)

x <- biomaRt::getBM(attributes = c("ensembl_gene_id",
                                   "entrezgene_accession",
                                   "entrezgene_id",
                                   #  "uniprot_gn_id",
                                   "external_gene_name"),#"chromosome_name","start_position","end_position"#"name_1006""uniprot_gn_symbol"
                    filters = "entrezgene_id",#"ensembl_gene_id",
                    values = entrzID,#file_to_read$ensembl_gene_id,
                    mart = ensembl);gc()

#SAVING BIOMART TABLE
write.csv(x,paste0(tableFolder_H,"/","biomart_orig_table.csv"),row.names=F,quote=F)
#########################
#OBTAINING HOW MANY GENES HAVE ONE TO MANY RELATIONSHIPS!
x_count <- lapply(1:length(entrzID),function(i){
  x_count_n <- length(unique(x[which(x$entrezgene_id==as.numeric(entrzID[[i]])),]$ensembl_gene_id))
  x_count <- as.data.frame(matrix(nrow=1,ncol = 2))
  x_count[,1] <- as.numeric(entrzID[[i]])
  x_count[,2] <- x_count_n
  return(x_count)
})
x_count <- do.call(rbind, x_count)
x_count <- x_count[which(x_count[,2]!=1),]
colnames(x_count) <- c("entrezgene_id","count")

#########################
#GET INFORMATION FROM NCG WEBSITE TO GET RELATIONSHIP ONE TO ONE AND AVOID ONE TO MANY. THUS, REMOVING NOISE
x_missing <- lapply(1:nrow(x_count),function(i){
cat(i,"\n")
  if(!file.exists(paste0(logFolder_H,"/",entrzID[[i]],"_NCG.csv"))){
    theurl <- paste0("http://ncg.kcl.ac.uk/query.php?gene_name=",x_count$entrezgene_id[[i]])
    ensembl_id_table <- as.data.frame(matrix(nrow=1,ncol=2))
    ensembl_id_table[,1] <- entrzID[[i]]
    colnames(ensembl_id_table) <- c("entrezgene_id","ensembl_gene_id")
#READ HTML
    file <- xml2::read_html(theurl)
    Sys.sleep(3)
#file <- minimal_html(file)
#GETTING TABLE USING HTML RVEST R PACKAGE
    tables <- rvest::html_node(file, "table")
    table1 <- rvest::html_table(tables, fill = T,trim = T)
    table1 <- table1[-c(1,2),1]
    table1 <- unique(table1)
    dt <- data.table::data.table(table1)
    dt <- dt[, c("IDS", "VALUE") := tstrsplit(table1, ":", fixed=TRUE)]
    dt <- dt[,-1]
    ensembl_id_table[,2] <- dt[which(dt$IDS=="Ensembl Gene ID"),2]
    rm(dt,tables,table1,theurl,file);gc()
    write.csv(ensembl_id_table,paste0(logFolder_H,"/",entrzID[[i]],"_NCG.csv"),row.names = F,quote = F)
  } else {
    cat(paste(entrzID[[i]],"downloaded..."),"\n")
    ensembl_id_table <- read.csv(paste0(logFolder_H,"/",entrzID[[i]],"_NCG.csv"),header = T)
  }
  cat(paste(i,"done!"),"\n")
  return(ensembl_id_table)
})

#JOINING UNIQUE IDs
x_missing <- do.call(rbind,x_missing)
x_missing$ensembl_gene_id <- as.character(x_missing$ensembl_gene_id)
x_missing <- x_missing[complete.cases(x_missing),]
x_missing$entrezgene_id <- trimws(x_missing$entrezgene_id)
x_missing$ensembl_gene_id <- trimws(x_missing$ensembl_gene_id)

#GETTING JUST UNIQUE IDs
x2_no_dup <- x
x2_no_dup <- x2_no_dup[(x2_no_dup$entrezgene_id %notin% x_count$entrezgene_id),]
#GETTING DUPLICATE IDs
x2_dup <- x
x2_dup <- x2_dup[(x2_dup$entrezgene_id %in% x_count$entrezgene_id),]
#MATCHING INFORMATION FROM WEB SCRAPPING TO AVOID DUPLICATES
x2_match <- x2_dup[(x2_dup$ensembl_gene_id %in% x_missing$ensembl_gene_id),]
x_final <- rbind(x2_no_dup,x2_match)

write.csv(x_final,paste0(tableFolder_H,"/","template_not_dups.csv"),row.names=F,quote=F)

#########################
#########################
#GETTING PATHWAYS INFO FROM PANTHER
# cols <- c("CLASS_TERM","CLASS_ID","SPECIES","PATHWAY_TERM","PATHWAY_ID")
# res_inner <- PANTHER.db::select(PANTHER.db, keys=as.character(x_final$entrezgene_id), columns=cols, keytype="ENTREZ")
# res_inner$entrezgene_id <- res_inner$ENTREZ; 
# res_inner$entrezgene_id <- as.numeric(res_inner$entrezgene_id)
# res_panther_human <- dplyr::left_join(res_inner, x,"entrezgene_id" )
# res_panther_human$ENTREZ <- NULL
# 
# write.csv(tableFolder,paste0(output_dir,"/","biomart_table_not_dups_panther.csv"),row.names=F,quote=F)
#########################################################################################################

blastn = "E:/NCBI/blast-2.10.0+/bin/blastn" # blast-2.2.30+/
#blast_db = "E:/JAVERIANA/CANCER/blast_db/TAR10_ARAB_cDNA"
blast_db = "E:/JAVERIANA/CANCER/blast_db/ARAB/ARAB_cDNA"
evalue = 10
format = 6

colnames <- c("qseqid",
              "sseqid",
              "pident",
              "length",
              "mismatch",
              "gapopen",
              "qstart",
              "qend",
              "sstart",
              "send",
              "evalue",
              "bitscore")

###########################################################################
######EXTRACTING SEQUENCE FROM ENSEMBL AND RUNNING LOCAL BLAST
ensembl_gene_id_unique <- as.character(unique(x_final$ensembl_gene_id)) #UNIQUE ENSEMBL IDS

# #COPY BETWEEN FOLDER IF THEY ARE READY
# dir1 <- "E:/JAVERIANA/CANCER/workspace/out/FASTA_ENSEMBL2"
# dir2 <- "E:/JAVERIANA/CANCER/workspace/out/FASTA_ENSEMBL"
# file.copy(paste0(dir1,"/",x_final$ensembl_gene_id,".fasta"),
#           paste0(dir2,"/",x_final$ensembl_gene_id,".fasta")) 

##GETTING BEST HIT PER SEQUENCE
blast_seq <- lapply(1:length(ensembl_gene_id_unique),function(i){
  
  cat(i,"\n")
  if(!file.exists(paste0(SeqFolder_H,"/",ensembl_gene_id_unique[[i]],".fasta"))){
    seq = biomaRt::getSequence(id=ensembl_gene_id_unique[[i]],
                               type="ensembl_gene_id",
                               seqType="cdna", mart = ensembl)
    seq <- seq[[1]][[1]]
    
    seq <- trimws(seq)
    seq <- Biostrings::DNAStringSet(x=seq); names(seq) <- ensembl_gene_id_unique[[i]]
    Biostrings::writeXStringSet(seq, filepath=paste0(SeqFolder_H,"/",ensembl_gene_id_unique[[i]],".fasta"), append=F,
                                compress=F, compression_level=NA, format="fasta")
    Sys.sleep(10)
  } else {
    seq <- Biostrings::readDNAStringSet(paste0(SeqFolder_H,"/",ensembl_gene_id_unique[[i]],".fasta"))
  }
  
  
  input = paste0(SeqFolder_H,"/",ensembl_gene_id_unique[[i]],".fasta")
  output = paste0(xmlFolder_H,"/",ensembl_gene_id_unique[[i]],".out")
  
  #RUNNING LOCAL BLAST FROM SHELL
  if(!file.exists(output)){
    
    blast_out <- base::system2(command = blastn,
                               args = c("-db", blast_db,
                                        "-query", input,
                                        "-outfmt", format,
                                        "-out", output,
                                        "-evalue", evalue,
                                        #"-num_alignments",500,
                                        "-num_threads",4,
                                        "-word_size",11,
                                        "-max_target_seqs",1000,
                                        "-gapopen",5,
                                        "-gapextend",2,
                                        "-penalty",-3,
                                        "-reward",2,
                                        "-max_hsps",100,
                                        "-soft_masking","true",#"-soft_masking","false",
                                        "-strand","both",
                                        "-perc_identity",10,
                                        #"-culling_limit",0
                                        "-dust","yes",
                                        "-html",
                                        "-lcase_masking"
                                        # "-remote"
                                        #"-window_masker_taxid",3702
                                        # "-ungapped"
                               ),
                               wait = TRUE,
                               stdout = TRUE) %>%
      as_tibble() %>% 
      separate(col = value, 
               into = colnames,
               sep = "\t",
               convert = TRUE)
    
  } else {
    cat("BLAST RESULTS ALREADY AVAILABLE","\n")
    #    if(length(readLines(output))>0){
    if(file.info(output)$size>0){
      blast_out <- as_tibble(read.table(output))
      colnames(blast_out) <- colnames
    } else {
      blast_out <- as_tibble(data.frame(matrix(nrow=0,ncol = length(colnames))))
      colnames(blast_out) <- colnames
    }
  }
  
  #OMMITING EMPTY RESULTS
  if(nrow(blast_out)==0){#if(identical( blast_out, character(0))){
    blast_out <- NULL
  } else {
    blast_out <- blast_out #read.table(textConnection(blast_out))
  }
  return(blast_out)
})


##GETTING BEST HIT PER SEQUENCE
for(i in 1:length(blast_seq)){
  blast_seq[[i]] <- as.data.frame(blast_seq[[i]][1,])
};rm(i)

#JOINING BLAST RESULTS IN A UNIQUE TABLE

blast_seq <- do.call(rbind,blast_seq)
blast_seq2 <- blast_seq
blast_seq$ensembl_gene_id <- blast_seq$qseqid 
blast_seq$ensembl_gene_id <- as.character(blast_seq$ensembl_gene_id)
# blast_seq_final <- dplyr::inner_join(x,blast_seq,by=c("ensembl_gene_id"))
#colnames(blast_seq_final)[3] <- "entrez"
blast_seq$tair <- blast_seq$sseqid
blast_seq$tair <- stri_sub(blast_seq$tair, 1, -3)

write.csv(blast_seq,paste0(tableFolder_H,"/","blast_HUMAN_ARAB_best_hits.csv"),row.names=F,quote=F)



#USING RENTREZ TO GET SEQUENCES
# write.csv(x,paste0(path,"/","biomaRt.csv"),row.names=F,quote=F)
#entrzPUBID <-  unique(file_to_read$pubmed_id)

# x_list <-  lapply(1:length(entrzID),function(i){
#     cat(paste(i, "starting"),"\n")
#     if(!file.exists(paste0(SeqFolder_ENTREZ,"/",entrzID[[i]],".fasta"))){
#       x_query <- entrez_link(dbfrom="gene", id=entrzID[[i]], db="nucleotide",api_key=key_NCBI)
#       Sys.sleep(1)
#       if(!is.null(x_query$links$gene_nuccore_refseqgene)){
#         all_recs <- entrez_fetch(db="nucleotide", id=x_query$links$gene_nuccore_refseqgene, rettype="fasta",api_key=key_NCBI)
#         write(all_recs, file=paste0(SeqFolder_ENTREZ,"/",entrzID[[i]],".fasta"))
#         Sys.sleep(1)
#         cat(paste(i, "done!"),"\n")
#         x=(paste(i,"|done"))
#       } else {
#         x=(paste(i,"|no refseq"))
#       }
#     } else {
#       cat(paste(entrzID[[1]],"ALREADY DOWNLOADED"),"\n")
#       x=(paste(i,"|download"))
#     }
#     return(x)
#   })

# x_count <- lapply(1:length(entrzID),function(i){
#   x_count_n <- length(unique(x[which(x$entrezgene_id==as.numeric(entrzID[[i]])),]$ensembl_gene_id))
#   x_count <- as.data.frame(matrix(nrow=1,ncol = 2))
#   x_count[,1] <- as.numeric(entrzID[[i]])
#   x_count[,2] <- x_count_n
#   return(x_count)
# })
# x_count <- do.call(rbind, x_count)
# x_count <- x_count[which(x_count[,2]!=1),]