# Cancer hallmarks project

This project is an effort to try to find orthologues genes related to human cancer in Arabidopsis thaliana to use the plant as research model.
This set of R script use a tsv file to develop the next steps:
*  First, search the GO term associated with the genes annotated into the file.
*  Second, this pipeline will use biomaRt library to access to ensembl information and find entrez ids and UNIPROT Ids to use in pantherdb library.
*  Third, this pipeline will get the fasta sequence from ensembl 
*  Four, this pipeline will perform using BLAST local database built from TAIR database files.

This repo needs a TAIR and ARAB blast db made from  files Araport11_genes.201606.cdna and TAIR10_cdna_20101214_updated.fasta downloaded from https://www.arabidopsis.org/download/index-auto.jsp?dir=%2Fdownload_files%2FSequences

Requirements: 

*  BLAST+ https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/

*  BLAST DBs commands:

1.  makeblastdb -in E:\JAVERIANA\CANCER\blast_db\INPUT\Araport11_genes.201606.cdna.fasta -dbtype nucl -parse_seqids -out E:\JAVERIANA\CANCER\blast_db\ARAB\ARAB_cDNA -title ARAB11

2.  makeblastdb -in E:\JAVERIANA\CANCER\blast_db\INPUT\TAIR10_cdna_20101214_updated.fasta -dbtype nucl -parse_seqids -out E:\JAVERIANA\CANCER\blast_db\TAR\TAR10_ARAB_cDNA -title TAR


R libraries: 


*  library(biomaRt)
*  library(org.At.tair.db) #OPTIONAL
*  library(rentrez)
*  library(PANTHER.db)
*  library(AnnotationHub)
*  library(tidyr)
*  library(dplyr)
*  library("hoardeR") #OPTIONAL
*  library(stringr)
*  library(Biostrings)
